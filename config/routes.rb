Rails.application.routes.draw do
  resources :documents, only: [:index, :create, :new, :show]
  devise_for :users
  resources :policies
  resources :companies
  resources :employees
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "documents#index"
end
