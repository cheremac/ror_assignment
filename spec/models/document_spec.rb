require 'rails_helper'

RSpec.describe Document, :type => :model do
  let(:user) {
    User.create!(email: "name#{rand(100)}@company.com",
    password: "Password",
    password_confirmation: "Password")
  }

  subject {
    described_class.new(company_id: Company.create(name: 'Company Name').id,
    file: File.open("#{Rails.root}/spec/fixtures/valid_sample.csv"),
    user_id: User.first )
  }

  it "is valid with valid attributes" do
    expect(subject).to be_valid
  end

  it "is not valid without a title" do
    subject.file = nil
    expect(subject).to_not be_valid
  end
end
