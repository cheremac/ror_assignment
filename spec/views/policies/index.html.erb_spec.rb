require 'rails_helper'

RSpec.describe "policies/index", :type => :view do
  let(:company) { Company.create(name: 'Company Name New') }
  before(:each) do
    assign(:policies, [
      Policy.create!(
        :name => "Name",
        :company => company
      ),
      Policy.create!(
        :name => "Policy 2",
        :company => company
      )
    ])
  end

  it "renders a list of policies" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 1
    assert_select "tr>td", :text => nil.to_s, :count => 0
  end
end
