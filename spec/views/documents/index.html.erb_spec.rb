require 'rails_helper'

RSpec.describe "documents/index", type: :view do

  let(:valid_attributes) {
    {
      company_id: Company.create(name: 'Company Name').id,
      file: File.open("#{Rails.root}/spec/fixtures/valid_sample.csv"),
      user_id: User.first
    }
  }

  before(:each) do
    assign(:documents, [
      Document.create!(valid_attributes),
      Document.create!(valid_attributes)
    ])
  end

  it "renders a list of documents" do
    render
  end
end
