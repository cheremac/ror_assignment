require 'rails_helper'

RSpec.describe "employees/index", :type => :view do
  before(:each) do
    assign(:employees, [
      Employee.create!(
        :name => "Name",
        :email => "name#{rand(100)}@company.com",
        :phone => "Phone",
        :company => Company.create(name: 'Company Name')
      ),
      Employee.create!(
        :name => "Name",
        :email => "name#{rand(100)}@company.com",
        :phone => "Phone",
        :company => Company.create(name: 'Company Name')
      )
    ])
  end

  it "renders a list of employees" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 0
  end
end
