class EmployeeCsvJob < ApplicationJob
  require 'csv'
  queue_as :default

  def perform(employee_file)
    company = employee_file.company
    row_errors = []
    batch_size   = 2000

    File.open(employee_file.file.path) do |file|
      file.lazy.each_slice(batch_size) do |lines|
        CSV.parse(lines.join, :headers => true, :header_converters => :symbol ) do |row|
          begin
            process_employee(row, company, row_errors)
          rescue Exception => error
            row_errors << error.message + " For: #{row}"
          ensure
            next
          end
          employee_file.error = row_errors
          employee_file.save
        end
      end
    end
  end

  private

  def process_employee(row, company, row_errors)
    employee = company.employees.find_or_initialize_by(email: row[:email])
    employee.name = row[:employee_name]
    employee.phone = row[:phone]

    if row[:report_to].present?
      employee_manager = Employee.process_manager(row, company)
    end

    if employee.save
      employee.policy_ids = Employee.process_policies(row, company, employee)
      employee.move_to_child_of(employee_manager) if employee_manager.present?
    else
      row_errors << employee.errors.messages
    end
  end
end
