require 'carrierwave/orm/activerecord'
class Document < ApplicationRecord
  serialize :error

  belongs_to :company
  mount_uploader :file, FileUploader

  validates :company_id, :file, presence: true
end
