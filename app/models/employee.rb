class Employee < ApplicationRecord
  belongs_to :company
  has_and_belongs_to_many :policies

  validates :name, presence: true
  validates :email, uniqueness: true

  acts_as_nested_set

  class << self
    def process_manager(row, company)
      employee_manager = company.employees.find_or_initialize_by(email: row[:report_to])
      if employee_manager.new_record?
        employee_manager.name = row[:report_to].split('@').first
        employee_manager.save
      end
      employee_manager
    end

    def process_policies(row, company, employee)
      policies_list = row[:assigned_policies].split('|').map { |policy| policy.downcase }
      policies_list = policies_list.map do |policy_name|
        company.policies.find_or_create_by(name: policy_name, company_id: company.id)
      end
      policies_list.collect(&:id).compact
    end
  end
end
