class DocumentsController < ApplicationController
  before_action :authenticate_user!

  def index
    @documents = current_user.documents.includes(:company)
  end

  def new
    @file = current_user.documents.build
  end

  def show
    @file = Document.find(params[:id])
  end

  def create
    @file = current_user.documents.build(document_params)
    if @file.save
      EmployeeCsvJob.perform_later(@file)
      redirect_to documents_path, notice: 'File Uploaded Sucessfully.'
    else
      render action: :new
    end
  end

  private
  # Never trust parameters from the scary internet, only allow the white list through.
  def document_params
    params.require(:document).permit(:company_id, :file)
  end
end
