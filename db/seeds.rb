# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Creating default user to upload Files
User.delete_all
user = User.new
user.email = 'test@example.com'
user.password = "Test@123"
user.password_confirmation = "Test@123"
user.save!
puts "User Email: 'test@example.com' and User Password: 'Test@123'"


# Create Dummy Comanies
Company.delete_all
companies = [{name: 'Comapny 1'},{name: 'Comapny 2'}, {name: 'Comapny 3'}]
Company.create(companies)
