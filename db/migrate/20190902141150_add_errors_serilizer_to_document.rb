class AddErrorsSerilizerToDocument < ActiveRecord::Migration[5.2]
  def change
    add_column :documents, :error, :text
  end
end
