class AddCompanyAndUserAssotiationToDocument < ActiveRecord::Migration[5.2]
  def change
    add_column :documents, :company_id, :integer
    add_column :documents, :user_id, :integer
  end
end
